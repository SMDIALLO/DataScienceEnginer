/*** The number of paid payments not using several instalments (use payment.type) per month ***/

SELECT
	   EXTRACT(MONTH FROM created) as month,

       COUNT(id) AS count
FROM payment
WHERE [type] = 'DIRECT_DEBIT'
GROUP BY EXTRACT(MONTH FROM created)

/*** The number of payments paid using several instalments and the induced amount in � per merchant name ***/


SELECT 
       COUNT(payment.id), 
	   amount, 
	   name

FROM payment
JOIN shop ON
payment.shop_id = shop.id
WHERE [type] in ('3_TIMES', '4_TIMES')

GROUP BY name


/***  The running total of all payments per year only for shops of retail activity ***/

SELECT
       EXTRACT(YEAR, shop.created),
       SUM(payment.amount)
FROM payment
JOIN shop ON
payment.shop_id = shop.id
WHERE shop.activity = 'retail'
GROUP BY EXTRACT(YEAR, shop.created)


/***  The number of payments with at least one 'debit done' event per merchant  ***/






/***  The full export of payments info with shop name, activity and the associated number of logs  ***/


SELECT
       shop.name,
       shop.activity,
	   payment_log.log_content

FROM shop
JOIN payment ON
shop.id = payment.shop_id
JOIN payment_log ON
payment.id = payment_log.payment_id


/***  
Sensitive data can be encrypted.
For example, if we receive a table A which contains sensitive data such as telephone number,
e-mail, bank account, we encrypt them to obtain a table B. So the analysts will have access to table B.
Once they have done the analysis, the real information can be retrieved by applying the 
reverse encryption function (for those who are authorised).



***/
