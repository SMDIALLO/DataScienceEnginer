# -*- coding: utf-8 -*-
"""
Created on Sat Apr  2 09:34:09 2022

@author: pc
"""
import pandas as pd
from sqlalchemy import create_engine
import psycopg2 
from better_profanity import profanity

### Extract the link from the internet


link = "https://github.com/fivethirtyeight/data/blob/master/hip-hop-candidate-lyrics/genius_hip_hop_lyrics.csv"
df = pd.read_html(link, match = "candidate")

def extract(link):
    return pd.read_html(link, match = "candidate")

### Transform: Remove bad words,
# Processing step
dff = df[0]
def f(text):
    return profanity.censor(text)

dff["line"] = dff["line"].apply(f)
dff = dff.drop(columns=["Unnamed: 0"])


    

Lyrics = dff[["id", "line"]]
Artist = dff[["id", "song", "album_release_date", "theme"]]


### Load data in Posgres

conn_string = 'postgres://postgres:sidy1996@localhost/MyDatabase'
db = create_engine(conn_string)
conn = psycopg2.connect()

def load(df, conn):
    try:
        df.to_sql('df', con=conn, if_exists='replace',
              index=False)
        conn.autocommit = True
        
        print("df load successfully ")
    
    except (Exception, psycopg2.Error) as error:
        print("Failed to load df table", error)

    finally:
        # closing database connection.
        if conn:
            conn.close()
            print("PostgreSQL connection is closed")
            

load(Lyrics, conn)

load(Artist, conn)


#############     The DAG      ###################

from airflow import DAG
from airflow.operators.python import PythonOperator
from datetime import datetime

with DAG("my_dag", start_date = datetime(2022, 1, 1),
    schedule_interval="@daily") as dag:

        Extract_data = PythonOperator(
            task_id = "Extract_Data_url",
            python_callable = extract
        )

        Transform_data = PythonOperator(
            task_id="Tranform_Data",
            python_callable= Processing
        )

        Load_data = PythonOperator(
            task_id = "Load_Data_posgres",
            python_callable = load
        )

        

        Extract_data >> Transform_data >> Load_data




    